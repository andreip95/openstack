class openstack::memcached {
  class { 'memcached':
    max_memory => '12%',
    listen_ip => $::ipaddress_eth0
  }
}
