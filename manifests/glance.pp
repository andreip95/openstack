class openstack::glance {
  require 'openstack::mysql_db'
  
  exec { 'glance_user':
    command => "/bin/openstack user create --domain default \
              --password glance glance",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"      
  }

  exec { 'glance_role_to_proj_and_user':
    command => "/bin/openstack role add --project service --user glance admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"
  }

  exec { 'glance_service':
    command => "/bin/openstack service create --name glance \
              --description \"OpenStack Image service\" image",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"
  }

  exec { 'glance_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              image public http://openstack:9292",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"
  }

  exec { 'glance_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              image admin http://openstack:9292",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"
  }

  exec { 'glance_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              image internal http://openstack:9292",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/glance-finished"
  }

  package { 'openstack-glance':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-glance':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-glanceclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/glance/glance-api.conf":
    mode => "0644",
    owner => 'glance',
    group => 'glance',
    source => 'puppet:///modules/openstack/configs/glance/glance-api.conf'
  }

  file { "/etc/glance/glance-registry.conf":
    mode => "0644",
    owner => 'glance',
    group => 'glance',
    source => 'puppet:///modules/openstack/configs/glance/glance-registry.conf'
  }

  exec { 'glance_db':
    command => "/bin/glance-manage db_sync",
    user => "glance",
    unless => "/bin/ls /root/glance-finished"
  }

  service { 'openstack-glance-api':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-glance-registry':
    ensure => 'running',
    enable => true
  }

  file { '/root/glance-finished':
    ensure => "file"
  }
}
