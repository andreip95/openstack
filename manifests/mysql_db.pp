class openstack::mysql_db {
  mysql::db { 'keystone':
    user     => 'keystone',
    password => 'keystone',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'glance':
    user     => 'glance',
    password => 'glance',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'nova':
    user     => 'nova',
    password => 'nova',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'nova_api':
    user     => 'nova',
    password => 'nova',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'neutron':
    user     => 'neutron',
    password => 'neutron',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'cinder':
    user     => 'cinder',
    password => 'cinder',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { 'heat':
    user      => 'heat',
    password  => 'heat',
    host      => '%',
    grant     => ['ALL']
  }
}
