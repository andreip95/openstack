class openstack::environment {
  exec { 'liberty_repo': 
    command => "/bin/yum-config-manager --add-repo http://distro.ctinetworks.com/distro/mirror/centos/7.3.1611/cloud/x86_64/openstack-liberty/"
  }

  package { 'python-openstackclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-selinux':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openvswitch':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  service { 'openvswitch':
    ensure => "running",
    enable => true
  }

  exec { 'br_add':
    command => "/bin/ovs-vsctl add-br cluster01"
  }

  package { 'wget':
    ensure => "installed"
  }
}
