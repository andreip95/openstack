class openstack::neutron {
  require 'openstack::mysql_db'
  
  exec { 'neutron_user':
    command => "/bin/openstack user create --domain default \
              --password neutron neutron",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"      
  }

  exec { 'neutron_role_to_proj_and_user':
    command => "/bin/openstack role add --project service --user neutron admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"
  }

  exec { 'neutron_service':
    command => "/bin/openstack service create --name neutron \
              --description \"OpenStack Networking\" network",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"
  }

  exec { 'neutron_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              neutron public http://openstack:9696",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"
  }

  exec { 'neutron_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              neutron admin http://openstack:9696",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"
  }

  exec { 'neutron_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              neutron internal http://openstack:9696",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/neutron-finished"
  }

  package { 'openstack-neutron':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-neutron-ml2':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-neutron-linuxbridge':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-neutronclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'ebtables':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'ipset':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/neutron/neutron.conf":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/neutron.conf'
  }

  file { "/etc/neutron/plugins/ml2/ml2_conf.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/ml2_conf.ini'
  }

  file { "/etc/neutron/plugins/ml2/linuxbridge_agent.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/linuxbridge_agent.ini'
  }

  file { "/etc/neutron/l3_agent.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/l3_agent.ini'
  }

  file { "/etc/neutron/dhcp_agent.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/dhcp_agent.ini'
  }

  file { "/etc/neutron/dnsmasq-neutron.conf":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/dnsmasq-neutron.conf'
  }

  file { "/etc/neutron/metadata_agent.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack/configs/neutron/metadata_agent.ini'
  }

  exec { 'ml2_symlink':
    command => "/bin/ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini",
    unless => "/bin/ls /etc/neutron/plugin.ini"
  }

  exec { 'neutron_db':
    command => "/bin/neutron-db-manage --config-file /etc/neutron/neutron.conf \
              --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head",
    user => "neutron",
    unless => "/bin/ls /root/neutron-finished"
  }

  service { 'neutron-server':
    ensure => 'running',
    enable => true
  }

  service { 'neutron-linuxbridge-agent':
    ensure => 'running',
    enable => true
  }

  service { 'neutron-dhcp-agent':
    ensure => 'running',
    enable => true
  }

  service { 'neutron-metadata-agent':
    ensure => 'running',
    enable => true
  }

  file { '/root/neutron-finished':
    ensure => "file"
  }
}
