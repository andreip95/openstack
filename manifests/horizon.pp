class openstack::horizon {
  require 'openstack::mysql_db'
  
  package { 'openstack-dashboard':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/openstack-dashboard/local_settings":
    mode => "0644",
    owner => "root",
    group => "apache",
    source => 'puppet:///modules/openstack/configs/horizon/local_settings'
  }

  file { "/etc/httpd/conf.d/openstack-dashboard.conf":
    mode => "0644",
    owner => "root",
    group => "apache",
    source => 'puppet:///modules/openstack/configs/horizon/openstack-dashboard.conf'
  }

  exec { 'httpd_restart':
    command => "/sbin/service httpd restart"
  }
}
