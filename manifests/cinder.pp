class openstack::cinder {
  require 'openstack::mysql_db'
  
  exec { 'cinder_user':
    command => "/bin/openstack user create --domain default \
              --password cinder cinder",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"      
  }

  exec { 'cinder_role_to_proj_and_user':
    command => "/bin/openstack role add --project service --user cinder admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_service_v1':
    command => "/bin/openstack service create --name cinder \
              --description \"OpenStack Block Storage\" volume",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_service_v2':
    command => "/bin/openstack service create --name cinderv2 \
              --description \"OpenStack Block Storage\" volumev2",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v1_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              volume public http://openstack:8776/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v1_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              volume admin http://openstack:8776/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v1_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              volume internal http://openstack:8776/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v2_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              volumev2 public http://openstack:8776/v2/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v2_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              volumev2 admin http://openstack:8776/v2/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  exec { 'cinder_v2_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              volumev2 internal http://openstack:8776/v2/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/cinder-finished"
  }

  package { 'openstack-cinder':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-cinderclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/cinder/cinder.conf":
    mode => "0644",
    owner => 'cinder',
    group => 'cinder',
    content => epp('openstack/cinder.conf.epp')
  }

  exec { 'cinder_db':
    command => "/bin/cinder-manage db sync",
    user => "cinder",
    unless => "/bin/ls /root/cinder-finished"
  }

  service { 'openstack-cinder-api':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-cinder-scheduler':
    ensure => 'running',
    enable => true
  }

  file { '/root/cinder-finished':
    ensure => "file"
  }
}
