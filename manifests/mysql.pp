class openstack::mysql {
  class { '::mysql::server':
    root_password => 'root',
    remove_default_accounts => true,
    override_options => {
      'mysqld' => {
        'bind-address' => $::ipaddress_eth0
      }
    }
  }
}
