class openstack::rabbitmq {

  package { 'rabbitmq-server':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  service { 'rabbitmq-server':
    ensure => "running",
    enable => "true"
  }

  exec { 'rabbitmq_user': 
    command => "/sbin/rabbitmqctl add_user openstack openstack",
    unless => "/sbin/rabbitmqctl list_users | /bin/grep openstack"
  }

  exec { 'rabbitmq_grant':
    command => "/sbin/rabbitmqctl set_permissions openstack \".*\" \".*\" \".*\""
  }
}
