class openstack::heat {
  require 'openstack::mysql_db'
  
  exec { 'heat_user':
    command => "/bin/openstack user create --domain default \
              --password heat heat",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"      
  }

  exec { 'heat_role_to_proj_and_user':
    command => "/bin/openstack role add --project service --user heat admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_service':
    command => "/bin/openstack service create --name heat \
              --description \"Orchestration\" orchestration",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_service_cfn':
    command => "/bin/openstack service create --name heat-cfn \
              --description \"Orchestration\" cloudformation",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              orchestration public http://openstack:8004/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              orchestration admin http://openstack:8004/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              orchestration internal http://openstack:8004/v1/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'cfn_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              cloudformation public http://openstack:8000/v1",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'cfn_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              cloudformation admin http://openstack:8000/v1",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'cfn_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              cloudformation internal http://openstack:8000/v1",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_domain':
    command => "/bin/openstack domain create --description \"Stack projects and users\" heat",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_domain_admin':
    command => "/bin/openstack user create --domain heat --password heat heat_domain_admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_role_to_domain_and_heat_admin':
    command => "/bin/openstack role add --domain heat --user heat_domain_admin admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_stack_owner_role':
    command => "/bin/openstack role create heat_stack_owner",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_stack_owner_to_demo_project':
    command => "/bin/openstack role add --project demo --user demo heat_stack_owner",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  exec { 'heat_stack_user_role':
    command => "/bin/openstack role create heat_stack_user",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/heat-finished"
  }

  package { 'openstack-heat-api':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-heat-api-cfn':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-heat-engine':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-heatclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/heat/heat.conf":
    mode => "0644",
    owner => 'heat',
    group => 'heat',
    source => 'puppet:///modules/openstack/configs/heat/heat.conf'
  }

  exec { 'heat_db':
    command => "/bin/heat-manage db_sync",
    user => "heat",
    unless => "/bin/ls /root/heat-finished"
  }

  service { 'openstack-heat-api':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-heat-api-cfn':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-heat-engine':
    ensure => 'running',
    enable => true
  }

  file { '/root/heat-finished':
    ensure => "file"
  }
}
