class openstack::nova {
  require 'openstack::mysql_db'
  
  exec { 'nova_user':
    command => "/bin/openstack user create --domain default \
              --password nova nova",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/nova-finished"        
  }

  exec { 'nova_role_to_proj_and_user':
    command => "/bin/openstack role add --project service --user nova admin",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/nova-finished"
  }

  exec { 'nova_service':
    command => "/bin/openstack service create --name nova \
              --description \"OpenStack Compute service\" compute",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/nova-finished" 
  }

  exec { 'nova_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              compute public http://openstack:8774/v2/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/nova-finished" 
  }

  exec { 'nova_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              compute admin http://openstack:8774/v2/%\(tenant_id\)s",
    environment => ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/nova-finished" 
  }

  exec { 'nova_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              compute internal http://openstack:8774/v2/%\(tenant_id\)s",
    environment=> ["OS_PROJECT_DOMAIN_ID=default", "OS_USER_DOMAIN_ID=default", "OS_PROJECT_NAME=admin", "OS_TENANT_NAME=admin", "OS_USERNAME=admin", "OS_PASSWORD=admin", "OS_AUTH_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
   unless => "/bin/ls /root/nova-finished" 
  }
  package { 'openstack-nova-api':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-nova-cert':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-nova-conductor':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-nova-console':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-nova-novncproxy':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-nova-scheduler':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-novaclient':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/nova/nova.conf":
    mode => "0644",
    owner => 'nova',
    group => 'nova',
    content => epp('openstack/nova.conf.epp')
  }

  exec { 'nova_db':
    command => "/bin/nova-manage db sync",
    user => "nova",
    unless => "/bin/ls /root/nova-finished" 
  }

  service { 'openstack-nova-api':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-nova-cert':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-nova-consoleauth':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-nova-scheduler':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-nova-conductor':
    ensure => 'running',
    enable => true
  }

  service { 'openstack-nova-novncproxy':
    ensure => 'running',
    enable => true
  }

  file { '/root/nova-finished':
    ensure => "file"
  }
}
