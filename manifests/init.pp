class openstack {
  include 'openstack::mysql'
  include 'openstack::mysql_db'
  include 'openstack::memcached'
  include 'openstack::environment'
  include 'openstack::rabbitmq'
  include 'openstack::keystone'
  include 'openstack::glance'
  include 'openstack::neutron'
  include 'openstack::cinder'
  include 'openstack::nova'
  include 'openstack::heat'
  include 'openstack::horizon'
}
