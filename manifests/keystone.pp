class openstack::keystone {
  require 'openstack::mysql_db'
  
  package { 'openstack-keystone':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'httpd':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'mod_wsgi':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'python-memcached':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/keystone/keystone.conf":
    mode => "0644",
    owner => 'keystone',
    group => 'keystone',
    source => 'puppet:///modules/openstack/configs/keystone/keystone.conf'
  }

  exec { 'keystone_db':
    command => "/bin/keystone-manage db_sync",
    user => "keystone",
    unless => "/bin/ls /root/keystone-finished"
  }

  file { "/etc/httpd/conf.d/wsgi-keystone.conf":
    mode => "0644",
    owner => "root",
    group => "root",
    source => 'puppet:///modules/openstack/configs/keystone/wsgi-keystone.conf'
  }

  file { "/etc/httpd/conf/httpd.conf":
    mode => "0644",
    owner => "root",
    group => "root",
    source => 'puppet:///modules/openstack/configs/keystone/httpd.conf'
  }

  exec { 'wsgi_public_permissions':
    command => "/bin/chown keystone:keystone /usr/bin/keystone-wsgi-public"
  }

  exec { 'wsgi_admin_permissions':
    command => "/bin/chown keystone:keystone /usr/bin/keystone-wsgi-admin"
  }

  service { 'httpd':
    ensure => running,
    enable => true,
  }

  exec { 'keystone_service_create':
    command => "/bin/openstack service create \
              --name keystone --description \"OpenStack Identity\" identity",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'keystone_endpoint_public':
    command => "/bin/openstack endpoint create --region RegionOne \
              identity public http://openstack:5000/v2.0",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'keystone_endpoint_internal':
    command => "/bin/openstack endpoint create --region RegionOne \
              identity internal http://openstack:5000/v2.0",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'keystone_endpoint_admin':
    command => "/bin/openstack endpoint create --region RegionOne \
              identity admin http://openstack:35357/v2.0",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'admin_project':
    command => "/bin/openstack project create --domain default \
              --description \"Admin Project\" admin",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'admin_user':
    command => "/bin/openstack user create --domain default \
              --password admin admin",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"      
  }

  exec { 'admin_role':
    command => "/bin/openstack role create admin",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"

  }

  exec { 'admin_role_to_proj_and_user':
    command => "/bin/openstack role add --project admin --user admin admin",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'service_project':
    command => "/bin/openstack project create --domain default \
              --description \"Service Project\" service",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'demo_project':
    command => "/bin/openstack project create --domain default \
              --description \"Demo Project\" demo",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'demo_user':
    command => "/bin/openstack user create --domain default \
              --password demo demo",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'user_role':
    command => "/bin/openstack role create user",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  exec { 'user_role_to_proj_and_user':
    command => "/bin/openstack role add --project demo --user demo user",
    environment => ["OS_TOKEN=a3a9e42d2fa03682d950", "OS_URL=http://openstack:35357/v3", "OS_IDENTITY_API_VERSION=3"],
    unless => "/bin/ls /root/keystone-finished"
  }

  file { "/usr/share/keystone/keystone-dist-paste.ini":
    mode => "0644",
    owner => "keystone",
    group => "keystone",
    source => 'puppet:///modules/openstack/configs/keystone/keystone-dist-paste.ini'
  }

  file { "/etc/keystone/admin-openrc.sh":
    mode => "0644",
    owner => "keystone",
    group => "keystone",
    source => 'puppet:///modules/openstack/configs/keystone/admin-openrc.sh'
  }

  file { '/root/keystone-finished':
    ensure => "file"
  }
}
